import { TestBed } from '@angular/core/testing';

import { InMemoryDatasService } from './in-memory-datas.service';

describe('InMemoryDatasService', () => {
  let service: InMemoryDatasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InMemoryDatasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
